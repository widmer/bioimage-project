// close previous runs
run("Close All");
root_folder=getDir("Choose the \"code\" directory from the cloned repo");
// those folders should have come with the git repo
folders = newArray("real/", "labeled/");
file_prefix="20160201_GFP_PH_5_w1sdcGFPquad_t";
// set the measurement for Analyze Particles...
run(
	"Set Measurements...",
	"area center perimeter shape stack display redirect=None decimal=3"
);


function preprocess() { 
	// denoising and contrast enhancement
	run("Non-local Means Denoising", "sigma=15 smoothing_factor=1.25 auto stack");
	run("Enhance Contrast...", "saturated=0.25 process_all use");
}

function mask() {
	// thresholding an morphological operations
	setThreshold(3855, 10280, "raw");
	run("Make Binary", "method=Default background=Dark black");
	setOption("BlackBackground", true);
	run("Fill Holes", "stack");
	run("Erode", "stack");
	run("Watershed", "stack");
	run("Erode", "stack");
}

function open_time_frame(i, folder) {
	// opens one 3d stack for time=i
	file=file_prefix+i+".stk";
	open(root_folder + folder + file);
}

function save_time_frame(i, folder) {
	// saves the 3d stack for time=i
	file=file_prefix+i+".tif";
	saveAs("Tiff", root_folder + folder + file);
}

function AP() {
	// Analyze Particles and labels them with a grayscale overlay
	options_label="size=0.0015-0.06 circularity=0.10-1.00" +
		"show=[Nothing] display exclude clear include overlay add stack";
	run("Analyze Particles...", options_label);
	roc = roiManager("count");
	if (roc > 0) {
		// scales the regions to cover the boundaries when labeling
		RoiManager.scale(1.25, 1.25, true);
		color_coder_options="measurement=XM " +
			"lut=Grays width=0 opacity=100 " +
			"label=cm^2 range=0.2-Max n.=15 " +
			"decimal=0 ramp=[512 pixels] font=SansSerif font_size=14 draw";
		run("ROI Color Coder", color_coder_options);
		close();
	}
}

function process_time_stack(t) {
	// runs all the pipeline for t time frames
	// set i = t to process a specific time frame
	for (i = 1; i <= t; i++) {
		open_time_frame(i, folders[0]);
		preprocess();
		mask();
		selectWindow(file_prefix+i+".stk");
		labeling(i);
		save_time_frame(i, folders[1]);
		run("Close All");
	}
}
function labeling(i) {
	// logic to run Analyze Particles and grayscale coloring slice-wise
	// to be able to flatten the grayscale overlay without overlap
	file_name=file_prefix+i+"-00";
	run("Stack to Images");
	for (i = 1; i <= 27; i++) {
		roc = roiManager("count");
		if (roc > 0) {
			roiManager("delete");
		}
		if (i < 10) {
			selectWindow(file_name+"0"+i);
		} else {
			selectWindow(file_name+i);
		}
		AP();
		if (i < 10) {
			selectWindow(file_name+"0"+i);
		} else {
			selectWindow(file_name+i);
		}
		roc = roiManager("count");
		if (roc > 0) {
			run("From ROI Manager");
		}
		run("Flatten");
	}	
	for (i = 1; i <= 27; i++) {
		if (i < 10) {
			selectWindow(file_name+"0"+i);
		} else {
			selectWindow(file_name+i);
		}
		close();
	}
	run("Images to Stack");
}

// modify the argument, between 1 and 40
// to run a custom number of time frames
t=40;
process_time_stack(t);

lf1 = root_folder+folders[1]+file_prefix+"1.tif";
run("Bio-Formats Importer", "open="+lf1+" autoscale color_mode=Grayscale group_files rois_import=[ROI manager] view=Hyperstack stack_order=XYCZT dimensions axis_1_number_of_images=2 axis_1_axis_first_image=1 axis_1_axis_increment=1 contains=[] name=labels_"+lf1);
run("Split Channels");
run("Close");
run("Close");







