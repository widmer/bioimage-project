// script adapted from the wiki: https://imagej.net/plugins/limeseg
// original source: https://github.com/NicoKiaru/LimeSeg/blob/master/src/main/resources/script-templates/LimeSeg/MacroSegCElegans_1Timepoint.ijm
// enable LimSeg and IJPB-Plugins update sites!
run("Close All");
run("Show GUI"); // Initializes LimeSeg
run("Clear all"); // Clear previous LimeSeg outputs
// Fetch C Elegans image
root_folder=getDir("Choose the \"code\" directory from the cloned repo");
r_folder="real/";
m_folder="measurements/";
file_prefix="20160201_GFP_PH_5_w1sdcGFPquad_t";

Dialog.create("Choose a time frame");
Dialog.addMessage("You can choose which time frame to visualize, between 1 and 40");
min=1;
max=40;
default=1;
Dialog.addSlider("Time frame", min, max, default);
// Finally show the GUI, once all parameters have been added
Dialog.show();
t = Dialog.getNumber();

open(root_folder+r_folder+"20160201_GFP_PH_5_w1sdcGFPquad_t"+t+".stk");
// Stores Image ID
idImage=getImageID();
run("Non-local Means Denoising", "sigma=10 smoothing_factor=1.5 auto stack");
run("Enhance Contrast...", "saturated=2 normalize process_all use");

// Looks for seeds:
sf = file_prefix+t+".stk";

//	Duplicates, Blur, Binarize, Find connected components and computes centers of mass
//waitForUser("Duplicates, Blur, Binarize, Find connected components and computes centers of mass");
run("Duplicate...", "title="+sf+" duplicate");
run("Gaussian Blur 3D...", "x=1 y=1 z=0.5");
run("Invert", "stack");
run("Enhance Contrast...", "saturated=5 normalize process_all");
run("3D Objects Counter", "threshold=42630 slice=13 min.=10 max.=1241460 exclude_objects_on_edges objects centres_of_masses statistics");

// Closes uselesse image
close();

// Radius of the initial seed for LimeSeg
radius=15;

// Stores results into ROI Manager
//waitForUser("Store results into ROI Manager");
//IJ.renameResults("Results");
r_file="Results_"+t+".csv";
Table.rename("Statistics for "+sf, "Results");


for (i=0;i<nResults;i++) {
	xp=getResult("XM", i);	
	yp=getResult("YM", i);	
	zp=getResult("ZM", i);
	setSlice(zp+1);	
	makeOval(xp-radius,yp-radius,2*radius,2*radius);
	Roi.setPosition(1, zp+1, 1);
	roiManager("Add");
}
close();
// Prepare to work on the initial CElegans image
selectImage(idImage);


run("Sphere Seg", "d_0=3.5 f_pressure=0.03 z_scale=3.125 range_in_d0_units=1.5 samecell=false show3d=true numberofintegrationstep=-1 realxypixelsize=0.32");

saveAs("Results", root_folder+m_folder+r_file);
