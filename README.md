# C.elegans embryo development measurement
## a BIO-410 Bioimage Informatics Project
Ulysse Widmer, Mariia Eremina, Nicolas Zaugg

[[_TOC_]]

## Project
The goal of this project is to segment, track and mesure the volumes of C.elegans embryo cells while they divide. The first goal is to segment and label each cells, then track their division, and finally report the evolution of the volume of each cell.

## Code
Our [code](code/) is contained in the `code` directory. It consists of 3 ImageJ macro files, `labels.ijm`, `boundaries.ijm` and `3d-visualization.ijm`, which allow the respective actions implied by their names.

The labeling macro computes the labels of the cells across all time frames and displays a 4D stack of the labels. The boundaries macro is similar but displays a 4D stack of the boundaries, and finally the 3D visualization macro uses the LimSeg plugin to display a labeled 3D visualization of the one choosen time frame, as well as the informations that could have been useful to complete volume tracking (but we didn't have the time).

## Presentation (slides)
You can find the presentation of our work in the slides [`presentation.pdf`](presentation.pdf).

## Sample data
The real-life data we use for the project can be found in the `code/real` directory. It contains 40 3-dimensional images, each one representing a time frame of 3D C.elegans embryo in development. You can generate all the results locally using our macro files.

## User manual
Check our [`user_manual.pdf`](user_manual.pdf) to know how to run everything smoothly!
